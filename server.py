import BaseHTTPServer
import os

class RequestHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    """ Handle HTTP requests by returning a fixed page"""
    HTDOCS = "/home/osirv/pi_labs/jurkovic/se_lab6/htdocs"

    def do_GET(self):
        if self.path == "/":
            self.path = "/index.html"
        page = self.handle_file(self.path)
        if page:
            self.send_content(200, page)
        else:
            self.handle_error(404, "File not found")

    def handle_file(self, file_path):
        full_path = self.HTDOCS + file_path
        print "File: ", full_path
        if os.path.isfile(full_path):
            with open(full_path, "rb") as reader:
                content= reader.read()
            return content
        else:
            return None

    def handle_error(self, status_code, message):
        page = "<html> <body> <p>"
        page += message
        page += "<p> </body> </html>"
        self.send_content(status_code, message)

    def send_content(self, status_code, content):
        self.send_response(status_code)
        self.send_header("Content-Typ", "text/html")
        self.send_header("Content.Length", str(len(content)))
        self.end_headers()
        self.wfile.write(content)

if __name__ == "__main__":
    print "Starting web server..."
    server_address = ('',8080)
    server = BaseHTTPServer.HTTPServer(server_address, RequestHandler)
    server.serve_forever()